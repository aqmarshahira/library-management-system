package com.example.LibraryManagementSystem;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.example.LibraryManagementSystem.adapter.LanguageSpinnerAdapter;
import com.example.LibraryManagementSystem.model.Book;
import com.example.LibraryManagementSystem.model.SharedPrefManager;
import com.example.LibraryManagementSystem.model.Users;
import com.example.LibraryManagementSystem.remote.ApiUtils;
import com.example.LibraryManagementSystem.remote.BookService;
import com.example.LibraryManagementSystem.remote.UserService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewBookActivity extends AppCompatActivity {


    private Spinner spSubject;
    private EditText etDescription;
    private Spinner spLecturerName;
    private static TextView tvDate; // static because need to be accessed by DatePickerFragment
    private static TextView tvTime; //private static Time time;
    private static Date dateConsult; // static because need to be accessed by DatePickerFragment

    private Context context;


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);

        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            // create a date object from selected year, month and day
            dateConsult = new GregorianCalendar(year, month, day).getTime();

            // display in the label beside the button with specific date format
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            tvDate.setText( sdf.format(dateConsult) );
        }
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar ct = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kuala_Lumpur")); // Set the time zone to Malaysia
            int hour = ct.get(Calendar.HOUR_OF_DAY); // Use HOUR_OF_DAY to get 24-hour format
            int minutes = ct.get(Calendar.MINUTE);

            // Create a new instance of timePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minutes, DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
            int hour = selectedHour;
            int minute = selectedMinute;
            String amPm = "AM"; // Initialize AM/PM to "AM"

            if (hour >= 12) {
                amPm = "PM";
                if (hour > 12) {
                    hour -= 12; // Convert to 12-hour format
                }
            }

            tvTime.setText(String.format(Locale.US, "%02d:%02d %s", hour, minute, amPm));
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_book);

        // store context
        context = this;

        // get view objects references
        spSubject = findViewById(R.id.spSubject);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.subject, R.layout.spinner_item);
        spSubject.setAdapter(adapter);

        spLecturerName = findViewById(R.id.spLecturerName);
        etDescription = findViewById(R.id.etDescription);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);
        // set default createdAt value, get current date
        dateConsult = new Date();
        // display in the label beside the button with specific date format
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        tvDate.setText( sdf.format(dateConsult) );

        //get lecturer username
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        // send request to add new book to the REST API
        UserService userService = ApiUtils.getUserService();
        Call<List<Users>> call = userService.getAllUserAdmin(users.getToken());
        // execute
        call.enqueue(new Callback<List<Users>>() {
            @Override
            public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {

                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // invalid session?
                if (response.code() == 401)
                    displayAlert("Invalid session. Please re-login");

                // consult added successfully
                List<Users> users = response.body();
                if (users != null) {

                    // set to spinner
                    LanguageSpinnerAdapter usa = new LanguageSpinnerAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, users);
                    spLecturerName.setAdapter(usa);
                } else {
                    displayAlert("Retrieve users failed.");
                }
            }

            @Override
            public void onFailure(Call<List<Users>> call, Throwable t) {
                displayAlert("Error [" + t.getMessage() + "]");
                // for debug purpose
                Log.d("MyApp:", "Error: " + t.getCause().getMessage());
            }
        });

    }
    /**
     * Called when pick date button is clicked. Display a date picker dialog
     * @param v
     */
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void addNewConsultation(View v) {

        // Get values from form fields
        String subject = spSubject.getSelectedItem().toString();
        String description = etDescription.getText().toString();
        String date = tvDate.getText().toString();
        String time = tvTime.getText().toString();

        // Check if any of the fields are empty
        if (subject.isEmpty() || description.isEmpty() || date.isEmpty() || time.isEmpty()) {
            // Show a Toast message indicating that all fields are required
            Toast.makeText(context, "All fields are required.", Toast.LENGTH_SHORT).show();
            return;
        }


        // get values in form
        Users lecturer = (Users) spLecturerName.getSelectedItem();

        //get user from sharedPreference
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        Log.d("MyApp:", "Response: " + users.getToken());
        // create a consultation object
        // set id to 0, it will be automatically generated by the db later
        // set image to dummy value
        Book c = new Book(0, subject , description, date , time , "New", users.getId() , lecturer.getId());
        Log.d("MyApp:", "Response: " + c.toString());

        // send request to add new book to the REST API
        BookService consultationService = ApiUtils.getConsultationService();
        Call<Book> call = consultationService.addConsultation(users.getToken(),c);

        // execute
        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {

                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // invalid session?
                if (response.code() == 401)
                    displayAlert("Invalid session. Please re-login");

                // book added successfully?
                Book addedConsultation = response.body();

                if (addedConsultation != null) {
                    // display message
                    Toast.makeText(context,
                            addedConsultation.getSubject() + " added successfully.",
                            Toast.LENGTH_LONG).show();

                    // end this activity and forward user to RequestListView
                    Intent intent = new Intent(context, RequestBookListView.class);
                    startActivity(intent);
                    finish();
                } else {
                    displayAlert("Add New Consultation failed.");
                }
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                displayAlert("Error [" + t.getMessage() + "]");
                // for debug purpose
                Log.d("MyApp:", "Error: " + t.getCause().getMessage());
            }
        });
    }

    /**
     * Displaying an alert dialog with a single button
     * @param message - message to be displayed
     */
    public void displayAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}