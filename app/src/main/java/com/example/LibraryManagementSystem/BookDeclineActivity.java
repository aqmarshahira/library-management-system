package com.example.LibraryManagementSystem;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.LibraryManagementSystem.model.*;
import com.example.LibraryManagementSystem.remote.*;
import com.example.LibraryManagementSystem.R;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDeclineActivity extends AppCompatActivity {

    private BookService consultationService;
    private Book consultation;      // store consultation info

    // form fields
    private EditText tvFeedbackDecline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_decline);

        // retrieve consultation id from intent
        // get consultation id sent by ConsultationListActivity, -1 if not found
        Intent intent = getIntent();
        int id = intent.getIntExtra("consultID", -1);

        // get references to the form fields in layout
        tvFeedbackDecline = findViewById(R.id.txtFeedbackDecline);

        // retrieve consultation info from database using the consultation id
        // get user info from SharedPreferences
        Users user = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // get consultation service instance
        consultationService = ApiUtils.getConsultationService();

        // execute the API query. send the token and consultation id
        consultationService.getConsultation(user.getToken(), id).enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // get consultation object from response
                consultation = response.body();

                // set values into forms
                tvFeedbackDecline.setText(consultation.getFeedback());
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                Toast.makeText(null, "Error connecting", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Update consultation info in database when the user click Decline button
     * @param view
     */
    public void updateConsultation(View view) {
        // get values in form
        String feedback = tvFeedbackDecline.getText().toString();
        String status = "Declined";

        // update the consultation object retrieved in onCreate with the new data. the consultation object
        // already contains the id
        consultation.setFeedback(feedback);
        consultation.setStatus(status);

        Log.d("MyApp:", "Consultation info: " + consultation.toString());

        // get user info from SharedPreferences
        Users user = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // send request to update the consultation record to the REST API
        BookService consultationService = ApiUtils.getConsultationService();
        Call<Book> call = consultationService.updateConsultation(user.getToken(), consultation);

        Context context = this;
        // execute
        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {

                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // invalid session
                if (response.code() == 401)
                    displayAlert("Invalid session. Please re-login");

                // consultation updated successfully
                Book updatedConsultation = response.body();
                if (updatedConsultation != null) {
                    // display message
                    Toast.makeText(context,
                            "Request decline updated successfully.",
                            Toast.LENGTH_LONG).show();

                    // end this activity and forward user to ConsultationListActivity
                    Intent intent = new Intent(context, BookListActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    displayAlert("Update Approval failed.");
                }
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                displayAlert("Error [" + t.getMessage() + "]");
                // for debug purpose
                Log.d("MyApp:", "Error: " + t.getCause().getMessage());
            }
        });
    }

    /**
     * Displaying an alert dialog with a single button
     * @param message - message to be displayed
     */
    public void displayAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}