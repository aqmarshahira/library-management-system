package com.example.LibraryManagementSystem;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.LibraryManagementSystem.model.Book;
import com.example.LibraryManagementSystem.model.SharedPrefManager;
import com.example.LibraryManagementSystem.model.Users;
import com.example.LibraryManagementSystem.remote.ApiUtils;
import com.example.LibraryManagementSystem.remote.BookService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDetailActivity extends AppCompatActivity {

    BookService consultationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);

        // get consultation id sent by ConsultationListActivity, -1 if not found
        Intent intent = getIntent();
        int id = intent.getIntExtra("consultID", -1);

        // get user info from SharedPreferences
        Users user = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // get consultationService instance
        consultationService = ApiUtils.getConsultationService();

        // execute the API query. send the token and book id
        consultationService.getConsultation(user.getToken(), id).enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // get consultation object from response
                Book consultation = response.body();

                // get references to the view elements
                TextView tvSubject = findViewById(R.id.txtSubject);
                TextView tvDesc = findViewById(R.id.txtDescription);
                TextView tvStudentName = findViewById(R.id.txtStudentName);
                TextView tvDate = findViewById(R.id.txtDate);
                TextView tvTime = findViewById(R.id.txtTime);
                TextView tvStatus = findViewById(R.id.txtStatus);
                TextView tvfeedback = findViewById(R.id.txtFeedback);


                // set values
                tvSubject.setText(consultation.getSubject());
                tvDesc.setText(consultation.getDescription());
                tvStudentName.setText(consultation.getStudent().getUsername());
                tvDate.setText(consultation.getDate());
                tvTime.setText(consultation.getTime());
                tvStatus.setText(consultation.getStatus());
                tvfeedback.setText(consultation.getFeedback());
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                Toast.makeText(null, "Error connecting", Toast.LENGTH_LONG).show();
            }
        });
    }
}