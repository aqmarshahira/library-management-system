package com.example.LibraryManagementSystem;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.LibraryManagementSystem.adapter.BookAdapter;
import com.example.LibraryManagementSystem.model.*;
import com.example.LibraryManagementSystem.remote.*;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookListActivity extends AppCompatActivity {

    BookService consultationService;
    Context context;
    RecyclerView consultationList;
    BookAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        // get reference to the RecyclerView consultationList
        consultationList = findViewById(R.id.consultationList);

        //register for context menu
        registerForContextMenu(consultationList);

        // update listview
        updateListView();
    }

    /**
     * Fetch data for ListView
     */
    private void updateListView() {
        // get user info from SharedPreferences
        Users user = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // get consultationService instance
        consultationService = ApiUtils.getConsultationService();

        // execute the call. send the user token when sending the query
        consultationService.getConsultationByLecturerID(user.getToken(), user.getId()).enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                List<Book> consultations;
                // token is not valid/expired
                if (response.code() == 401) {
                    displayAlert("Session Invalid");
                }
                else if (response.code() == 204) {
                    displayAlert("No consultations record");
                }
                else {
                    // Get list of consultation object from response

                    consultations = response.body();

                    // initialize adapter
                    adapter = new BookAdapter(getApplicationContext(), consultations);

                    // set adapter to the RecyclerView
                    consultationList.setAdapter(adapter);

                    // set layout to recycler view
                    consultationList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                    // add separator between item in the list
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(consultationList.getContext(),
                            DividerItemDecoration.VERTICAL);
                    consultationList.addItemDecoration(dividerItemDecoration);
                }


            }

            @Override
            public void onFailure(Call<List<Book>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error connecting to the server", Toast.LENGTH_LONG).show();
                displayAlert("Error [" + t.getMessage() + "]");
                Log.e("MyApp:", t.getMessage());
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.consultation_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Book selectedConsultation = adapter.getSelectedItem();
        Log.d("MyApp", "selected "+selectedConsultation.toString());
        int itemId = item.getItemId();

        if (itemId == R.id.menu_details) {
            // should match the id in the context menu file
            doViewDetails(selectedConsultation);
        } else if (itemId == R.id.menu_approve) {
            // should match the id in the context menu file
            doApprove(selectedConsultation);
        } else if (itemId == R.id.menu_decline) {
            // should match the id in the context menu file
            doDecline(selectedConsultation);
        }

        return super.onContextItemSelected(item);
    }

    private void doApprove(Book selectedConsultation) {
        // for debugging purpose
        Log.d("MyApp:", "launching update activity for "+selectedConsultation.toString());

        // launch UpdateBookActivity and pass the book id
        Intent intent = new Intent(getApplicationContext(), UpdateApproveActivity.class);
        intent.putExtra("consultID", selectedConsultation.getConsultID());
        startActivity(intent);
    }

    private void doDecline(Book selectedConsultation) {
        // for debugging purpose
        Log.d("MyApp:", "launching update activity for "+selectedConsultation.toString());

        // launch UpdateBookActivity and pass the book id
        Intent intent = new Intent(getApplicationContext(), BookDeclineActivity.class);
        intent.putExtra("consultID", selectedConsultation.getConsultID());
        startActivity(intent);
    }

    private void doViewDetails(Book selectedConsultation) {
        Log.d("MyApp:", "viewing details "+selectedConsultation.toString());
        Intent intent = new Intent(getApplicationContext(), BookDetailActivity.class);
        intent.putExtra("consultID", selectedConsultation.getConsultID());
        startActivity(intent);
    }

    /**
     * Displaying an alert dialog with a single button
     * @param message - message to be displayed
     */
    public void displayAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}