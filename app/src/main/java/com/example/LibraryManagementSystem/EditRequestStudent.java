package com.example.LibraryManagementSystem;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.example.LibraryManagementSystem.adapter.LanguageSpinnerAdapter;
import com.example.LibraryManagementSystem.remote.*;
import com.example.LibraryManagementSystem.model.*;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditRequestStudent extends AppCompatActivity {


    private BookService consultationService;
    private Book consultation;      // store consultation info

    // form fields
    private Spinner spSubj;
    private Spinner splectName;
    private EditText tDescription;
    private EditText tStuName;
    private static TextView tvEditDate; // static because need to be accessed by DatePickerFragment
    private static TextView tvEditTime;
    private static Date editDate; // static because need to be accessed by DatePickerFragment
    private static Time editTime;
    int hour, minute;

    private Context context;

    private LanguageSpinnerAdapter lecturerSpinnerAdapter;

    /**
     * Date picker fragment class
     * Reference: https://developer.android.com/guide/topics/ui/controls/pickers
     */
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {


            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // create a date object from selected year, month and day
            editDate = new GregorianCalendar(year, month, day).getTime();

            // display in the label beside the button with specific date format
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            tvEditDate.setText( sdf.format(editDate) );
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_request);

        //initialize spinner
        splectName = findViewById(R.id.splectName);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.lecturer, R.layout.spinner_item);
        splectName.setAdapter(adapter);

        // retrieve consult id from intent
        // -1 if not found
        Intent intent = getIntent();
        int id = intent.getIntExtra("consultID", -1);

        // get references to the form fields in layout
        spSubj = findViewById(R.id.spSubj);
        tDescription = findViewById(R.id.tDescription);
        splectName = findViewById(R.id.splectName);
        tStuName = findViewById(R.id.tStuName);
        tvEditDate = findViewById(R.id.tvEditDate);
        tvEditTime = findViewById(R.id.tvEditTime);

        // retrieve book info from database using the book id
        // get user info from SharedPreferences
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // retrieved list of user and set to spinner
        // send request to add new book to the REST API
        UserService userService = ApiUtils.getUserService();
        Call<List<Users>> call = userService.getAllUsers(users.getToken());
        // execute
        call.enqueue(new Callback<List<Users>>() {
            @Override
            public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {

                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // invalid session?
                if (response.code() == 401)
                    displayAlert("Invalid session. Please re-login");

                // consult date added successfully
                List<Users> users = response.body();
                if (users != null) {
                    // display message
                    Toast.makeText(getApplicationContext(),
                            "Successfully booked",
                            Toast.LENGTH_LONG).show();


                } else {
                    displayAlert("Retrieve users failed.");
                }
            }

            @Override
            public void onFailure(Call<List<Users>> call, Throwable t) {
                displayAlert("Error [" + t.getMessage() + "]");
                // for debug purpose
                Log.d("MyApp:", "Error: " + t.getCause().getMessage());
            }
        });


        // get consultationService instance
        consultationService = ApiUtils.getConsultationService();

        consultationService.getConsultation(users.getToken(), id).enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // get consultation object from response
                consultation = response.body();

                // set values into forms
                // set spinner. find the index of this subjectid in the spinner
                for (int i = 0; i< spSubj.getCount(); i++) {
                    if (spSubj.getSelectedItem().toString() == consultation.getSubject())
                        spSubj.setSelection(i);
                }

                tDescription.setText(consultation.getDescription());

                int lectId = consultation.getLecturer_id();

                // set spinner. find the index of this lectId in the spinner
                for (int i = 0; i< lecturerSpinnerAdapter.getCount(); i++) {
                    if (lecturerSpinnerAdapter.getItem(i).getId() == lectId)
                        splectName.setSelection(i);
                }
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                Toast.makeText(null, "Error connecting", Toast.LENGTH_LONG).show();
            }
        });

    }

    /**
     * Called when pick date button is clicked. Display a date picker dialog
     * @param v
     */
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    /**
     * Called when pick time button is clicked. Display a time picker dialog
     * @param v
     */
    public void showTimePickerDialog(View v) {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                hour = selectedHour;
                minute = selectedMinute;
                tvEditTime.setText(String.format(Locale.getDefault(), "%02d:%02d", hour, minute));
            }
        };

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, onTimeSetListener, hour, minute, true);
        timePickerDialog.setTitle("Select Time");
        timePickerDialog.show();
    }

    /**
     * Update consultation info in database when the user click update consultation button
     * @param view
     */
    public void editRequest(View view) {
        // get values in form
        String description = tDescription.getText().toString();
        Users lecturer = (Users) splectName.getSelectedItem();

        // convert createdAt date to format in DB
        // reference: https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date_consult = sdf.format(editDate);

        String time_consult = tvEditTime.getText().toString();
        String status = "New";

        //get spinner subject text
        String subject = spSubj.getSelectedItem().toString();

        // update the book object retrieved in onCreate with the new data. the book object
        // already contains the id
        consultation.setSubject(subject);
        consultation.setDescription(description);
        consultation.setLecturer_id(lecturer.getId());
        consultation.setDate(date_consult);
        consultation.setTime(time_consult);
        consultation.setStatus(status);

        Log.d("MyApp:", "Note info: " + consultation.toString());

        // get user info from SharedPreferences
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // send request to update the consultation record to the REST API
        BookService consultationService = ApiUtils.getConsultationService();
        Call<Book> call = consultationService.updateConsultation(users.getToken(), consultation);

        Context context = this;
        // execute
        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {

                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // invalid session
                if (response.code() == 401)
                    displayAlert("Invalid session. Please re-login");

                // consultation updated successfully
                Book updatedConsultation = response.body();
                if (updatedConsultation != null) {
                    // display message
                    Toast.makeText(context,
                            updatedConsultation.getSubject() + " updated successfully.",
                            Toast.LENGTH_LONG).show();

                    // end this activity and forward user to RequestListView
                    Intent intent = new Intent(context, RequestBookListView.class);
                    startActivity(intent);
                    finish();
                } else {
                    displayAlert("Update Request failed.");
                }
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                displayAlert("Error [" + t.getMessage() + "]");
                // for debug purpose
                Log.d("MyApp:", "Error: " + t.getCause().getMessage());
            }
        });
    }

    /**
     * Displaying an alert dialog with a single button
     * @param message - message to be displayed
     */
    public void displayAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}