package com.example.LibraryManagementSystem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.LibraryManagementSystem.model.SharedPrefManager;
import com.example.LibraryManagementSystem.model.Users;

public class MainActivityLecturer extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_lecturer);
        context = this;

        // get user info from SharedPreferences
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // assign action to logout button
        Button btnLogout = findViewById(R.id.btLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // clear the shared preferences
                SharedPrefManager.getInstance(getApplicationContext()).logout();

                // display message
                Toast.makeText(getApplicationContext(),
                        "You have successfully logged out.",
                        Toast.LENGTH_LONG).show();

                // forward to LoginActivity
                finish();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));

            }
        });

        // assign action to View Request button
        Button btView = findViewById(R.id.btView);
        btView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // forward user to ConsultationListActivity
                Intent intent = new Intent(context, BookListActivity.class);
                startActivity(intent);
            }
        });
    }
}