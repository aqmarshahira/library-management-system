package com.example.LibraryManagementSystem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.LibraryManagementSystem.adapter.BookAdapter;
import com.example.LibraryManagementSystem.model.Book;
import com.example.LibraryManagementSystem.model.DeleteResponse;
import com.example.LibraryManagementSystem.model.SharedPrefManager;
import com.example.LibraryManagementSystem.model.Users;
import com.example.LibraryManagementSystem.remote.ApiUtils;
import com.example.LibraryManagementSystem.remote.BookService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestBookListView extends AppCompatActivity {
    BookService consultationService;
    Context context;
    RecyclerView consultationList;
    BookAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requestbooklist_listview);

        // get reference to the RecyclerView consultationList
        consultationList = findViewById(R.id.consultationList);

        //register for context menu
        registerForContextMenu(consultationList);

        // update listview
        updateListView();

        // action handler for Add Consultation floating button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // forward user to NewConsultationActivity
                Intent intent = new Intent(getApplicationContext(), AddNewBookActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Fetch data for ListView
     */
    private void updateListView() {
        // get user info from SharedPreferences
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // get consultation service instance
        consultationService = ApiUtils.getConsultationService();

        // execute the call. send the user token when sending the query
        consultationService.getConsultationByStudentID(users.getToken(), users.getId()).enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                List<Book> consultations;
                // token is not valid/expired
                if (response.code() == 401) {
                    displayAlert("Session Invalid");
                } else if (response.code() == 204) {
                    displayAlert("No consultations record");
                } else {
                    // Get list of consultation objects from response
                    consultations = response.body();

                    // initialize adapter
                    adapter = new BookAdapter(getApplicationContext(), consultations);

                    // set adapter to the RecyclerView
                    consultationList.setAdapter(adapter);

                    // set layout to recycler view
                    consultationList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                    // add separator between items in the list
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(consultationList.getContext(),
                            DividerItemDecoration.VERTICAL);
                    consultationList.addItemDecoration(dividerItemDecoration);
                }
            }

            @Override
            public void onFailure(Call<List<Book>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error connecting to the server", Toast.LENGTH_LONG).show();
                displayAlert("Error [" + t.getMessage() + "]");
                Log.e("MyApp:", t.getMessage());
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.details_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Book selectedConsultation = adapter.getSelectedItem();
        Log.d("MyApp", "selected " + selectedConsultation.toString());
        int itemId = item.getItemId();

        if (itemId == R.id.details) {
            // should match the id in the context menu file
            doViewDetails(selectedConsultation);
        } else if (itemId == R.id.delete) {
            // should match the id in the context menu file
            doDeleteConsultation(selectedConsultation);
        }

        return super.onContextItemSelected(item);
    }

    private void doViewDetails(Book selectedConsultation) {
        Log.d("MyApp:", "viewing details " + selectedConsultation.toString());
        Intent intent = new Intent(getApplicationContext(), RequestDetailsActivity.class);
        intent.putExtra("consultID", selectedConsultation.getConsultID());
        startActivity(intent);
    }

    private void doEditConsultation(Book selectedConsultation) {
        Log.d("MyApp:", "viewing details " + selectedConsultation.toString());
        Intent intent = new Intent(getApplicationContext(), EditRequestStudent.class);
        intent.putExtra("consultID", selectedConsultation.getConsultID());
        startActivity(intent);
    }

    /**
     * Delete consultation record. Called by contextual menu "Delete"
     * @param selectedConsultation - consultation selected by user
     */
    private void doDeleteConsultation(Book selectedConsultation) {
        // get user info from SharedPreferences
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // prepare REST API call
        BookService consultationService = ApiUtils.getConsultationService();
        Call<DeleteResponse> call = consultationService.deleteConsultation(users.getToken(), selectedConsultation.getConsultID());

        // execute the call
        call.enqueue(new Callback<DeleteResponse>() {
            @Override
            public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {
                if (response.code() == 200) {
                    // 200 means OK
                    displayAlert("Consultation successfully deleted");
                    // update data in list view
                    updateListView();
                } else {
                    displayAlert("Consultation failed to delete");
                    Log.e("MyApp:", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DeleteResponse> call, Throwable t) {
                displayAlert("Error [" + t.getMessage() + "]");
                Log.e("MyApp:", t.getMessage());
            }
        });
    }

    /**
     * Displaying an alert dialog with a single button
     * @param message - message to be displayed
     */
    public void displayAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int consultID) {
                        //do things
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
