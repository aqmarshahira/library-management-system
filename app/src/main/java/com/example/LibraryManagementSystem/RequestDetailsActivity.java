package com.example.LibraryManagementSystem;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.LibraryManagementSystem.model.Book;
import com.example.LibraryManagementSystem.model.SharedPrefManager;
import com.example.LibraryManagementSystem.model.Users;
import com.example.LibraryManagementSystem.remote.ApiUtils;
import com.example.LibraryManagementSystem.remote.BookService;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestDetailsActivity extends AppCompatActivity {

    BookService consultationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);

        // get consult id sent by ConsultationListActivity, -1 if not found
        Intent intent = getIntent();
        int id = intent.getIntExtra("consultID", -1);

        // get user info from SharedPreferences
        Users users = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // get consultation service instance
        consultationService = ApiUtils.getConsultationService();

        // execute the API query. send the token and consult id
        consultationService.getConsultation(users.getToken(), id).enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                // for debug purpose
                Log.d("MyApp:", "Response: " + response.raw().toString());

                // get book object from response
                Book consultation = response.body();

                // get references to the view elements
                TextView vSubjName = (TextView)findViewById(R.id.tvSubj);
                TextView tvdesc = (TextView)findViewById(R.id.tvdesc);
                TextView vLectName = (TextView)findViewById(R.id.textView10);
                TextView vDate = (TextView)findViewById(R.id.tvDateReq);
                TextView vTime = (TextView)findViewById(R.id.textView12);
                TextView vStatus = (TextView)findViewById(R.id.textView13);
                TextView textView9 = (TextView)findViewById(R.id.textView9);


                // set values
                vSubjName.setText(consultation.getSubject());
                tvdesc.setText(consultation.getDescription());
                vLectName.setText(consultation.getLecturer().getUsername());
                textView9.setText(consultation.getStudent().getUsername());
                vDate.setText(consultation.getDate());
                vTime.setText(consultation.getTime());
                vStatus.setText(consultation.getStatus());

            }

            @Override
            public void onFailure(Call<Book> call, Throwable c) {
                Toast.makeText(null, "Error connecting", Toast.LENGTH_LONG).show();
            }
        });

    }

}