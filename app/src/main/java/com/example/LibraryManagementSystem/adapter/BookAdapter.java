package com.example.LibraryManagementSystem.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.LibraryManagementSystem.R;
import com.example.LibraryManagementSystem.model.Book;



import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {

    /**
     * Create ViewHolder class to bind list item view
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{

        public TextView subject;
        public TextView studentName;
        public TextView date;
        public TextView time;

        public ViewHolder(View itemView) {
            super(itemView);

            subject= (TextView) itemView.findViewById(R.id.subject);
            studentName = (TextView) itemView.findViewById(R.id.studentName);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            currentPos = getAdapterPosition(); //key point, record the position here
            return false;
        }
    }

    private List<Book> mListData;   // list of consultation objects
    private Context mContext;       // activity context
    private int currentPos;         //current selected position.

    public BookAdapter(Context context, List<Book> listData){
        mListData = listData;
        mContext = context;
    }

    private Context getmContext(){return mContext;}


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the single item layout
        View view = inflater.inflate(R.layout.book_list_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // bind data to the view holder
        Book m = mListData.get(position);
        holder.subject.setText(m.getSubject());
        holder.studentName.setText(m.getStudent().getUsername());
        holder.date.setText(m.getDate());
        holder.time.setText(m.getTime());
    }

    @Override
    public int getItemCount() {
        if(mListData!= null){
            return mListData.size();
        }
        else
            return 0;
    }

    public Book getSelectedItem() {
        if(currentPos>=0 && mListData!=null && currentPos<mListData.size()) {
            return mListData.get(currentPos);
        }
        return null;
    }


}
