package com.example.LibraryManagementSystem.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.LibraryManagementSystem.model.Users;

import java.util.List;

public class LanguageSpinnerAdapter extends ArrayAdapter<Users> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<Users> users;

    public LanguageSpinnerAdapter(Context context, int textViewResourceId,
                                  List<Users> users) {
        super(context, textViewResourceId, users);
        this.context = context;
        this.users = users;
    }

    @Override
    public int getCount(){
        return users.size();
    }

    @Override
    public Users getItem(int position){
        return users.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // get the current item using the values array (Users array) and the current position
        // reference each method that has been created in the bean object (Users class)
        label.setText(users.get(position).getFullname());

        // And finally return the dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(users.get(position).getFullname());

        return label;
    }
}


