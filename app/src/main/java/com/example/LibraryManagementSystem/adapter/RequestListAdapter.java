import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.LibraryManagementSystem.R;

public class RequestListAdapter extends ArrayAdapter {

    // normal constructor
    public RequestListAdapter(Activity context) {
        super(context, R.layout.student_request_listview);
        this.context = context;

    }

    // to reference the Activity where this ListView item is on
    private Activity context;

    // this will get called for each data items in the base data array
    public View getView(int position, View view, ViewGroup parent) {
// inflate one list item using single item layout
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.student_request_listview,
                null, true);
// change the values in the layout
// this code gets references to objects in the item_listview.xml file
        TextView vSubjName = (TextView)rowView.findViewById(R.id.nameSubject);
        TextView vLectName = (TextView)rowView.findViewById(R.id.nameLecturerName);
        TextView vDate = (TextView)rowView.findViewById(R.id.nameDate);
        TextView vTime = (TextView)rowView.findViewById(R.id.nameTime);
        TextView vStatus = (TextView)rowView.findViewById(R.id.nameStatus);


// return the single item view object for this item at index position
        return rowView;
    }

}

