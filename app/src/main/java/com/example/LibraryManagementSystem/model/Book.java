package com.example.LibraryManagementSystem.model;


public class Book {

    private int consultID;
    private String subject;
    private String description;
    private String date;
    private String time;
    private String status;
    private String feedback;
    private Users student;
    private Users lecturer;
    private int student_id;
    private int lecturer_id;

    public Book(){}

    public Book(int consultID, String subject, String description, String date, String time, String status, int student_id, int lecturer_id) {
        this.consultID = consultID;
        this.subject = subject;
        this.description = description;
        this.date = date;
        this.time = time;
        this.status = status;
        this.feedback = feedback;
        this.student = student;
        this.lecturer = lecturer;
        this.student_id = student_id;
        this.lecturer_id = lecturer_id;
    }

    public int getConsultID() {
        return consultID;
    }

    public void setConsultID(int consultID) {
        this.consultID = consultID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Users getStudent() {
        return student;
    }

    public void setStudent(Users student) {
        this.student = student;
    }

    public Users getLecturer() {
        return lecturer;
    }

    public void setLecturer(Users lecturer) {
        this.lecturer = lecturer;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public int getLecturer_id() {
        return lecturer_id;
    }

    public void setLecturer_id(int lecturer_id) {
        this.lecturer_id = lecturer_id;
    }

    @Override
    public String toString() {
        return "Book{" +
                "consultID=" + consultID +
                ", subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", status='" + status + '\'' +
                ", feedback='" + feedback + '\'' +
                ", student=" + student +
                ", lecturer=" + lecturer +
                ", student_id=" + student_id +
                ", lecturer_id=" + lecturer_id +
                '}';
    }
}

