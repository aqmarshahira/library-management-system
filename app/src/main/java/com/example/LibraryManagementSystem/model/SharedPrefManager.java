package com.example.LibraryManagementSystem.model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.LibraryManagementSystem.LoginActivity;

public class SharedPrefManager {

    //the constants
    private static final String SHARED_PREF_NAME = "bookstoresharedpref";
    private static final String KEY_ID = "keyid";
    private static final String KEY_USERNAME = "keyusername";
    private static final String KEY_EMAIL = "keyemail";
    private static final String KEY_TOKEN = "keygender";
    private static final String KEY_ROLE = "keyrole";
    private static final String KEY_FULLNAME = "keyfullname";
    private static final String KEY_CLASS = "keyclass";


    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    /**
     * method to let the user login
     * this method will store the user data in shared preferences
     * @param users
     */
    public void userLogin(Users users) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, users.getId());
        editor.putString(KEY_USERNAME, users.getUsername());
        editor.putString(KEY_EMAIL, users.getEmail());
        editor.putString(KEY_TOKEN, users.getToken());
        editor.putString(KEY_ROLE, users.getRole());
        editor.putString(KEY_FULLNAME, users.getFullname());
        editor.putString(KEY_CLASS, users.getClassStudent());
        editor.apply();
    }

    /**
     * this method will checker whether user is already logged in or not.
     * return True if already logged in
     */

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null) != null;
    }

    /**
     * this method will give the information of logged in user, retrieved from SharedPreferences
     */
    public Users getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);

        Users users = new Users();
        users.setId(sharedPreferences.getInt(KEY_ID, -1));
        users.setUsername(sharedPreferences.getString(KEY_USERNAME, null));
        users.setEmail(sharedPreferences.getString(KEY_EMAIL, null));
        users.setToken(sharedPreferences.getString(KEY_TOKEN, null));
        users.setRole(sharedPreferences.getString(KEY_ROLE, null));
        users.setFullname(sharedPreferences.getString(KEY_FULLNAME, null));
        users.setClassStudent(sharedPreferences.getString(KEY_CLASS, null));

        return users;
    }

    /**
     * this method will logout the user. clear the SharedPreferences
     */
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        mCtx.startActivity(new Intent(mCtx, LoginActivity.class));
    }
}