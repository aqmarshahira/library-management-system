package com.example.LibraryManagementSystem.model;

public class UpdateResponse {
    private int status;
    private Success success;


    public UpdateResponse() {
    }

    public UpdateResponse(int status, Success success) {
        this.status = status;
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "DeleteResponse{" +
                "status=" + status +
                ", success=" + success +
                '}';
    }
}

class Success2 {
    public int code;
    public String status;

    public Success2() {
    }

    public Success2(int code, String status) {
        this.code = code;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "Success{" +
                "code=" + code +
                ", status='" + status + '\'' +
                '}';
    }
}


