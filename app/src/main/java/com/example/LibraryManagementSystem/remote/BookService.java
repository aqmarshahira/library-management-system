package com.example.LibraryManagementSystem.remote;
import com.example.LibraryManagementSystem.model.*;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BookService {

    @GET("api/consultation")
    Call<List<Book>> getAllConsultation(@Header("api-key") String api_key);

    @GET("api/consultation/{consultID}")
    Call<Book> getConsultation(@Header("api-key") String api_key, @Path("consultID") int consultID);
    //Get consultation by student

    @GET("api/consultation")
    Call<List<Book>> getConsultationByStudentID(@Header("api-key") String api_key, @Query("student_id") int student_id);

    //Get consultation by student
    @GET("api/consultation")
    Call<List<Book>> getConsultationByLecturerID(@Header("api-key") String api_key, @Query("lecturer_id") int lecturer_id);


    /**
     * Add Consultation by sending a single Consultation JSON
     * @return Consultation object
     */
    @POST("api/consultation")
    Call<Book> addConsultation(@Header ("api-key") String apiKey, @Body Book consultation);

    /**
     * Delete Consultation based on the id
     * @return DeleteResponse object
     */
    @POST("api/consultation/delete/{consultID}")
    Call<DeleteResponse> deleteConsultation(@Header ("api-key") String apiKey, @Path("consultID") int consultID);

    /**
     * Update Consultation by sending a single Task JSON
     * @return task object
     */
    @POST("api/consultation/update")
    Call<Book> updateConsultation(@Header ("api-key") String apiKey, @Body Book consultation);

    /**
     * Update Consultation by sending task status
     * @return Consultation object
     */
    @PUT("api/consultation/update/{consultID}")
    Call<UpdateResponse> updateConsultationByID(@Header("api-key") String apiKey, @Path("consultID") int consultID);
}
