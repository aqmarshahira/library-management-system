package com.example.LibraryManagementSystem.remote;

import com.example.LibraryManagementSystem.model.Users;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserService {
    @FormUrlEncoded
    @POST("api/users/login")
    Call<Users> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/users/login")
    Call<Users> loginEmail(@Field("email") String email, @Field("password") String password);

    @POST("api/users")
    Call<Users> addUser(@Header("api-key") String apiKey, @Body Users users);

    @GET("api/users")
    Call<List<Users>> getAllUsers(@Header("api-key") String apiKey);

    @GET("api/users?role=admin")
    Call<List<Users>> getAllUserAdmin(@Header("api-key") String apiKey);
}

